﻿namespace BusinessObjectLayer.Entities
{
    public class MedicationMedicationPlan
    {
        public string MedicationId { get; set; }
        public Medication Medication { get; set; }
        public string MedicationPlanId { get; set; }
        public MedicationPlan MedicationPlan { get; set; }
    }
}
