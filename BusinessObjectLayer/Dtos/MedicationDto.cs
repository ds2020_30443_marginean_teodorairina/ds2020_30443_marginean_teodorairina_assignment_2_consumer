﻿namespace BusinessObjectLayer.Dtos
{
    public class MedicationDto
    {
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public string Dosage { get; set; }
    }
}
