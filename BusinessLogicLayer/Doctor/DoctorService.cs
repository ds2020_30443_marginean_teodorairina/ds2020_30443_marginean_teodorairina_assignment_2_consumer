﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessObjectLayer.Entities;
using DataAccessLayer.Repositories;

namespace BusinessLogicLayer.Doctor
{
    public class DoctorService : IDoctorService
    {
        private readonly IRepository<Medication> medicationRepository;

        public DoctorService(IRepository<Medication> medicationRepository)
        {
            this.medicationRepository = medicationRepository;
        }

        public async Task<ICollection<Medication>> GetAllMedications()
        {
            return await medicationRepository.GetAll();
        }

        public async Task<Medication> GetMedicationById(string id)
        {
            var medications = await medicationRepository.GetAll();

            return medications.FirstOrDefault(x => x.Id == id);
        }

        public async Task UpdateMedication(Medication medicationToUpdate)
        {
            await medicationRepository.Update(medicationToUpdate);
        }

        public async Task DeleteMedication(Medication medicationToDelete)
        {
            await medicationRepository.Delete(medicationToDelete);
        }

        public async Task CreateMedicationPlan(PatientEntity patient, MedicationPlan medicationPlan)
        {
            throw new System.NotImplementedException();
        }

        public async Task AddMedication(Medication medication)
        {
            await medicationRepository.Insert(medication);
        }
    }
}
