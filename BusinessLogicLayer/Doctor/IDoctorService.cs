﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessObjectLayer.Entities;

namespace BusinessLogicLayer.Doctor
{
    public interface IDoctorService
    {
        Task<ICollection<Medication>> GetAllMedications();
        Task<Medication> GetMedicationById(string id);
        Task AddMedication(Medication medication);
        Task UpdateMedication(Medication medicationToUpdate);
        Task DeleteMedication(Medication medicationToDelete);
        Task CreateMedicationPlan(PatientEntity patient, MedicationPlan medicationPlan);
    }
}
