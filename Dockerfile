#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["PresentationLayer/MedicationPlatform.API.csproj", "PresentationLayer/"]
COPY ["BusinessLogicLayer/BusinessLogicLayer.csproj", "BusinessLogicLayer/"]
COPY ["BusinessObjectLayer/BusinessObjectLayer.csproj", "BusinessObjectLayer/"]
COPY ["DataAccessLayer/DataAccessLayer.csproj", "DataAccessLayer/"]
RUN dotnet restore "PresentationLayer/MedicationPlatform.API.csproj"
COPY . .
WORKDIR "/src/PresentationLayer"
RUN dotnet build "MedicationPlatform.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MedicationPlatform.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
CMD ASPNETCORE_URLS=http://*:$PORT dotnet "MedicationPlatform.API.dll"