﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using AutoMapper;
using BusinessLogicLayer.Consumer;
using BusinessLogicLayer.Doctor;
using BusinessObjectLayer;
using BusinessObjectLayer.Entities;
using DataAccessLayer;
using DataAccessLayer.Repositories;
using MedicationPlatform.API.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace MedicationPlatform.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<DataContext>(optionsBuilder =>
                optionsBuilder.UseNpgsql(@"
                          User ID=ykaaxlcyivdxxy;Password=05b61971669e224023094d7f3d5a2475bcf01e066644eb5c2627bd0bd5d3dab0;Server=ec2-34-252-98-12.eu-west-1.compute.amazonaws.com;Port=5432;Database=dh439m1vnbeqt;Integrated Security=true;Pooling=true"));
            //services.AddDbContext<DataContext>(optionsBuilder =>
            //    optionsBuilder.UseNpgsql("User ID=postgres;Password=irina1234;Server=localhost;Port=5432;Database=medication_platform3;Integrated Security=true;Pooling=true;"));
            services.AddCors();
            services.AddGrpcHttpApi();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfiles());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddLogging(config =>
            {
                config.AddDebug();
                config.AddConsole();
            });

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication();
            services.AddAuthorization();


            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddSignalR();  

            services.AddScoped<IRepository<Medication>, BaseRepository<Medication>>();
           // services.AddScoped<IRepository<ActivityEntity>, BaseRepository<ActivityEntity>>();
            services.AddScoped<IRepository<MedicationPlan>, BaseRepository<MedicationPlan>>();
            services.AddScoped<IRepository<MedicationMedicationPlan>, BaseRepository<MedicationMedicationPlan>>();
            services.AddScoped<IDoctorService, DoctorService>();
            services.AddScoped<ActivityHub>();
            //services.AddSingleton<ConsumerService>();
            services.AddScoped<ConsumerService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            //app.UseHttpsRedirection();

            //app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ActivityHub>("/hub");
                endpoints.MapGrpcService<MedicationPlanDownloaderService>();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

            using (var scope = app.ApplicationServices.CreateScope())
            {
                InitializeDatabase(scope);
            }

            using (var scope = app.ApplicationServices.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<ConsumerService>().StartListening();
            }
        }

        private static void InitializeDatabase(IServiceScope serviceScope)
        {
            var services = serviceScope.ServiceProvider;

            try
            {
                SeedData.InitializeAsync(services).Wait();
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "Error occurred seeding the DB.");
            }
        }
    }
}
